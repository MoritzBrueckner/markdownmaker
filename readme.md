# ![](https://gitlab.com/MoritzBrueckner/markdownmaker/-/raw/master/logo/markdownmaker_icon.png)markdownmaker
[![PyPI](https://img.shields.io/pypi/v/markdownmaker?style=for-the-badge)](https://pypi.org/project/markdownmaker/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/markdownmaker?style=for-the-badge)](https://pypi.org/project/markdownmaker/)
[![PyPI - Downloads](https://img.shields.io/pypi/dw/markdownmaker?style=for-the-badge)](https://pypi.org/project/markdownmaker/)
[![PyPI - License](https://img.shields.io/pypi/l/markdownmaker?style=for-the-badge)](https://gitlab.com/MoritzBrueckner/markdownmaker/-/blob/master/LICENSE.md)
---
**markdownmaker** is an easy-to-use minimal library to generate a Markdown document with a Python API. Actually, this document you are reading right now was generated with markdownmaker for demonstration purposes ([source](https://gitlab.com/MoritzBrueckner/markdownmaker/-/blob/master/readmegen.py)). Currently it uses Github Flavored Markdown but more flavors might follow.

This library was developed to aid with generating a reference for [Armory3D](https://github.com/armory3d/armory)'s [logic nodes](https://github.com/armory3d/armory/wiki/reference), thus its feature scope is rather small and the output might have some bugs when creating complex documents. If you encounter problems, please open an issue.

## Installation
markdownmaker is installed via [pip](https://pypi.org/project/markdownmaker/):

```
pip install markdownmaker
```
## Usage (API)
Import the following:

```python
from markdownmaker.document import Document
from markdownmaker.markdownmaker import *
```
Then, create a document:

```python
doc = Document()
```
After you finished creating the document, `doc.write()` returns the Markdown source code.

### Emphasis
```python
doc.add(Paragraph(Italic("This text will be italic!")))
doc.add(Paragraph(Bold("This text will be bold!")))

doc.add(Paragraph(f"You can also combine {Bold(Italic('bold and italic text!'))}"))
```
*This text will be italic!*

**This text will be bold!**

You can also combine ***bold and italic text!***

### Headers
```python
doc.add(Header("This is a header"))
with HeaderSubLevel(doc):
    doc.add(Header("This is a sub-header"))
    with HeaderSubLevel(doc):
        doc.add(Header("This is a sub-sub-header"))
```
### This is a header
#### This is a sub-header
##### This is a sub-sub-header
### Lists
```python
doc.add(OrderedList((
    "Item 1", 
    "Item 2", 
    UnorderedList((
        Bold("Sub-item A"), 
        Italic("Sub-item B"))), 
    "Item 3")))
```
1. Item 1
2. Item 2
  - **Sub-item A**
  - *Sub-item B*
3. Item 3

### Horizontal Rule
```python
doc.add(HorizontalRule())
```
---
### Links and Images
```python
doc.add(Link(label='Go to top', url='#markdownmaker'))
doc.add(Image(url='https://gitlab.com/uploads/-/system/project/avatar/21351489/markdownmaker.png?width=40', alt_text='logo'))
```
[Go to top](#markdownmaker)

![logo](https://gitlab.com/uploads/-/system/project/avatar/21351489/markdownmaker.png?width=40)

### Code
```python
doc.add(CodeBlock("""import this
import __hello__""", language="python"))

doc.add(Paragraph(f"{InlineCode('Inline code')} is also supported!"))
```
```python
import this
import __hello__
```
`Inline code` is also supported!

### Quotes
```python
doc.add(Quote(f"""Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec et quam at eros dignissim accumsan. Aenean quis sagittis dolor. Ut justo nisl,
lobortis nec elit id, dictum ullamcorper ipsum. Fusce eu ullamcorper eros, nec
feugiat lectus.

{Italic(Bold("Albert Einstein."))}"""))
```
> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
> Donec et quam at eros dignissim accumsan. Aenean quis sagittis dolor. Ut justo nisl,
> lobortis nec elit id, dictum ullamcorper ipsum. Fusce eu ullamcorper eros, nec
> feugiat lectus.
> 
> ***Albert Einstein.***

## License
markdownmaker is licensed under the [zlib license](https://gitlab.com/MoritzBrueckner/markdownmaker/-/blob/master/LICENSE.md).

