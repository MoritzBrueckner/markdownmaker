from markdownmaker.document import Document
from markdownmaker.markdownmaker import *

badges = (
    # Alt text, Img source, Link URL
    ("PyPI", "https://img.shields.io/pypi/v/markdownmaker?style=for-the-badge", "https://pypi.org/project/markdownmaker/"),
    ("PyPI - Python Version", "https://img.shields.io/pypi/pyversions/markdownmaker?style=for-the-badge", "https://pypi.org/project/markdownmaker/"),
    ("PyPI - Downloads", "https://img.shields.io/pypi/dw/markdownmaker?style=for-the-badge", "https://pypi.org/project/markdownmaker/"),
    ("PyPI - License", "https://img.shields.io/pypi/l/markdownmaker?style=for-the-badge", "https://gitlab.com/MoritzBrueckner/markdownmaker/-/blob/master/LICENSE.md"),
)


def run():
    doc = Document()

    doc.add(Header(f"{InlineImage(url='https://gitlab.com/MoritzBrueckner/markdownmaker/-/raw/master/logo/markdownmaker_icon.png')}markdownmaker"))
    for badge in badges:
        doc.add(Link(label=f"{InlineImage(url=badge[1], alt_text=badge[0])}", url=badge[2]))
    doc.add(HorizontalRule())

    doc.add(Paragraph(
        f"{Bold('markdownmaker')} is an easy-to-use minimal library to generate a"
        f" Markdown document with a Python API."
        " Actually, this document you are reading right now was generated with"
        " markdownmaker for demonstration purposes"
        f" ({Link(label='source', url='https://gitlab.com/MoritzBrueckner/markdownmaker/-/blob/master/readmegen.py')})."
        " Currently it uses Github Flavored Markdown but more flavors might follow."))

    doc.add(Paragraph(
        "This library was developed to aid with generating a reference for"
        f" {Link(label='Armory3D', url='https://github.com/armory3d/armory')}'s"
        f" {Link(label='logic nodes', url='https://github.com/armory3d/armory/wiki/reference')},"
        " thus its feature scope is rather small and the output might have some"
        " bugs when creating complex documents. If you encounter problems, please"
        " open an issue."))

    with HeaderSubLevel(doc):
        doc.add(Header("Installation"))
        doc.add(Paragraph(f"markdownmaker is installed via {Link(label='pip', url='https://pypi.org/project/markdownmaker/')}:"))
        doc.add(CodeBlock("pip install markdownmaker", ""))

        doc.add(Header("Usage (API)"))

        doc.add(Paragraph("Import the following:"))
        doc.add(CodeBlock("""from markdownmaker.document import Document
from markdownmaker.markdownmaker import *""", "python"))

        doc.add(Paragraph("Then, create a document:"))
        doc.add(CodeBlock("doc = Document()", "python"))

        doc.add(Paragraph(f"After you finished creating the document, {InlineCode('doc.write()')} returns the Markdown source code."))

        with HeaderSubLevel(doc):
            doc.add(Header("Emphasis"))

            doc.add(CodeBlock("""doc.add(Paragraph(Italic("This text will be italic!")))
doc.add(Paragraph(Bold("This text will be bold!")))

doc.add(Paragraph(f"You can also combine {Bold(Italic('bold and italic text!'))}"))""", "python"))

            doc.add(Paragraph(Italic("This text will be italic!")))
            doc.add(Paragraph(Bold("This text will be bold!")))
            doc.add(Paragraph(f"You can also combine {Bold(Italic('bold and italic text!'))}"))

            doc.add(Header("Headers"))

            doc.add(CodeBlock("""doc.add(Header("This is a header"))
with HeaderSubLevel(doc):
    doc.add(Header("This is a sub-header"))
    with HeaderSubLevel(doc):
        doc.add(Header("This is a sub-sub-header"))""", "python"))

            doc.add(Header("This is a header"))
            with HeaderSubLevel(doc):
                doc.add(Header("This is a sub-header"))
                with HeaderSubLevel(doc):
                    doc.add(Header("This is a sub-sub-header"))

            doc.add(Header("Lists"))

            doc.add(CodeBlock("""doc.add(OrderedList((
    "Item 1", 
    "Item 2", 
    UnorderedList((
        Bold("Sub-item A"), 
        Italic("Sub-item B"))), 
    "Item 3")))""", "python"))

            doc.add(OrderedList((
                "Item 1",
                "Item 2",
                UnorderedList((
                    Bold("Sub-item A"),
                    Italic("Sub-item B"))),
                "Item 3")))

            doc.add(Header("Horizontal Rule"))

            doc.add(CodeBlock("""doc.add(HorizontalRule())""", "python"))

            doc.add(HorizontalRule())

            doc.add(Header("Links and Images"))

            doc.add(CodeBlock("""doc.add(Link(label='Go to top', url='#markdownmaker'))
doc.add(Image(url='https://gitlab.com/uploads/-/system/project/avatar/21351489/markdownmaker.png?width=40', alt_text='logo'))""", "python"))

            doc.add(Link(label='Go to top', url='#markdownmaker'))
            doc.add(Image(url='https://gitlab.com/uploads/-/system/project/avatar/21351489/markdownmaker.png?width=40', alt_text='logo'))

            doc.add(Header("Code"))

            doc.add(CodeBlock("""doc.add(CodeBlock(\"\"\"import this
import __hello__\"\"\", language="python"))

doc.add(Paragraph(f"{InlineCode('Inline code')} is also supported!"))""", "python"))

            doc.add(CodeBlock("""import this
import __hello__""", language="python"))
            doc.add(Paragraph(f"{InlineCode('Inline code')} is also supported!"))

            doc.add(Header("Quotes"))

            doc.add(CodeBlock("""doc.add(Quote(f\"\"\"Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec et quam at eros dignissim accumsan. Aenean quis sagittis dolor. Ut justo nisl,
lobortis nec elit id, dictum ullamcorper ipsum. Fusce eu ullamcorper eros, nec
feugiat lectus.

{Italic(Bold("Albert Einstein."))}\"\"\"))""", "python"))

            doc.add(Quote(f"""Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec et quam at eros dignissim accumsan. Aenean quis sagittis dolor. Ut justo nisl,
lobortis nec elit id, dictum ullamcorper ipsum. Fusce eu ullamcorper eros, nec
feugiat lectus.

{Italic(Bold("Albert Einstein."))}"""))

        doc.add(Header("License"))
        doc.add(Paragraph(f"markdownmaker is licensed under the {Link(label='zlib license', url='https://gitlab.com/MoritzBrueckner/markdownmaker/-/blob/master/LICENSE.md')}."))

    with open("readme.md", "w") as f:
        f.write(doc.write())


if __name__ == "__main__":
    run()
